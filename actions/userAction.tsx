import AsyncStorage from '@react-native-community/async-storage';

export const setUser = (user: object) => {
  if (user !== null) {
    AsyncStorage.setItem('user', JSON.stringify(user));
  } else {
    AsyncStorage.removeItem('user');
  }

  console.log("canceluser", user);

  return {
    type: 'SET_USER',
    user: user,
  };
};
