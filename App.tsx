import React, {Component} from 'react';
import configureStore from './store';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import Application from './Application';

const store = configureStore();

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <NavigationContainer>
          <Application />
        </NavigationContainer>
      </Provider>
    );
  }
}
