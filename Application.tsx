import React, {Component} from 'react';
import LoginScreen from './screens/LoginScreen';
import {connect} from 'react-redux';
import {setUser} from './actions/userAction';
import AsyncStorage from '@react-native-community/async-storage';
import {ActivityIndicator, SafeAreaView} from 'react-native';
import SecuredNavigator from './stacks/securedNavigator';

type ApplicationScreenProps = {
  user: object;
  onSetUser: any;
};

type ApplicationScreenState = {
  user: object;
  loading: boolean;
};

class Application extends Component<
  ApplicationScreenProps,
  ApplicationScreenState
> {
  constructor(
    props: ApplicationScreenProps | Readonly<ApplicationScreenProps>,
  ) {
    super(props);
    this.state = {
      loading: true,
      user: props.user,
    };
  }

  componentWillReceiveProps(
    nextProps: ApplicationScreenProps | Readonly<ApplicationScreenProps>,
  ) {
    this.setState({
      user: nextProps.user,
    });
  }

  async componentDidMount() {
    let user = await AsyncStorage.getItem('user');

    if (user !== null) {
      this.props.onSetUser(JSON.parse(user));
    }

    this.setState({
      loading: false,
    });
  }

  render() {
    if (this.state.loading) {
      return (
        <SafeAreaView
          style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator color={'#000000'} />
        </SafeAreaView>
      );
    }

    if (this.state.user !== null) {
      return <SecuredNavigator />;
    }

    return <LoginScreen />;
  }
}

export default connect(
  (state: {user: object}) => ({
    user: state.user,
  }),
  dispatch => ({
    onSetUser: (user: object) => {
      dispatch(setUser(user));
    },
  }),
)(Application);
