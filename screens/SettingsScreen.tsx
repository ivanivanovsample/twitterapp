import React, {Component} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';
import {setUser} from '../actions/userAction';

const {width} = Dimensions.get('window');

type SettingsScreenProps = {
  user: {
    id: number;
    username: string;
  };
  onSetUser: any;
};

type SettingsScreenState = {
  user: {
    id: number;
    username: string;
  };
};

class SettingsScreen extends Component<
  SettingsScreenProps,
  SettingsScreenState
> {
  constructor(props: SettingsScreenProps) {
    super(props);
    this.state = {
      user: props.user,
    };
  }

  logout() {
    this.props.onSetUser(null);
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Text style={styles.text}>ID: {this.state.user.id}</Text>
        <Text style={styles.text}>userame: {this.state.user.username}</Text>
        <TouchableOpacity
          style={styles.button}
          onPress={this.logout.bind(this)}>
          <Text style={styles.button_text}>Sign Out</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    marginTop: 20,
    height: 40,
    marginLeft: 30,
    marginRight: 30,
    width: width - 60,
    backgroundColor: '#000000',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button_text: {
    color: '#ffffff',
    fontSize: 14,
  },
  text: {
    fontSize: 19,
    marginTop: 12,
  },
});

export default connect(
  (state: {
    user: {
      id: number;
      username: string;
    };
  }) => ({
    user: state.user,
  }),
  dispatch => ({
    onSetUser: (user: object) => {
      dispatch(setUser(user));
    },
  }),
)(SettingsScreen);
