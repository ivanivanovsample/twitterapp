import React, {Component} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {connect} from 'react-redux';
import axios from 'axios';
import {twitter_token} from '../variables';

type TweetsScreenProps = {
  user: {
    id: number;
  };
  navigation: any;
};

type TweetsScreenState = {
  user: {
    id: number;
  };
  loading: boolean;
  tweets: any;
  next_token: any;
};

class TweetsScreen extends Component<TweetsScreenProps, TweetsScreenState> {
  constructor(props: TweetsScreenProps) {
    super(props);
    this.state = {
      user: props.user,
      loading: true,
      next_token: null,
      tweets: [],
    };
  }

  async componentWillMount() {
    let response = await axios.get(
      'https://api.twitter.com/2/users/' +
        this.state.user.id +
        '/tweets?max_results=20',
      {
        headers: {
          Authorization: 'Bearer ' + twitter_token,
        },
      },
    );

    console.log(response);

    const next_token = response.data.meta.next_token;

    this.setState({
      tweets: response.data.data,
      next_token,
      loading: false,
    });
  }

  async loadMore() {
    let response = await axios.get(
      'https://api.twitter.com/2/users/' +
        this.state.user.id +
        '/tweets?max_results=20&pagination_token=' +
        this.state.next_token,
      {
        headers: {
          Authorization: 'Bearer ' + twitter_token,
        },
      },
    );

    console.log(response);

    const next_token = response.data.meta.next_token;

    this.setState({
      tweets: this.state.tweets.concat(response.data.data),
      next_token,
    });
  }

  render() {
    let react = this;

    if (this.state.loading) {
      return (
        <SafeAreaView
          style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator color={'#000000'} />
        </SafeAreaView>
      );
    }

    return (
      <SafeAreaView style={styles.container}>
        <FlatList
          data={this.state.tweets}
          renderItem={({item}) => (
            <TouchableOpacity
              style={styles.tweet}
              onPress={() => {
                react.props.navigation.navigate('TweetScreen', {
                  tweet: item,
                });
              }}>
              <Text style={styles.tweet_id}>ID: {item.id}</Text>
              <Text style={styles.tweet_text}>Text: {item.text}</Text>
            </TouchableOpacity>
          )}
          onEndReached={this.loadMore.bind(this)}
          onEndReachedThreshold={0.5}
          initialNumToRender={20}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tweet: {
    borderColor: '#000000',
    borderStyle: 'solid',
    borderWidth: 1,
    width: '100%',
    padding: 20,
    marginBottom: 20,
  },
  tweet_id: {
    fontSize: 14,
  },
  tweet_text: {
    fontSize: 12,
  },
});

export default connect((state: {user: {id: number}}) => ({
  user: state.user,
}))(TweetsScreen);
