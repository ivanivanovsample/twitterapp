import React, {Component} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  ActivityIndicator,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import {setUser} from '../actions/userAction';
import {twitter_token} from '../variables';
import axios from 'axios';
import validTwitteUser from '../helpers';

const {width} = Dimensions.get('window');

type LoginScreenProps = {
  onSetUser: any
};

type LoginScreenState = {
  nickname: string;
  loading: boolean;
};

class LoginScreen extends Component<LoginScreenProps, LoginScreenState> {
  constructor(props: LoginScreenProps) {
    super(props);
    this.state = {
      nickname: '',
      loading: false,
    };
  }

  async submit() {
    let react = this;

    if (
      !validTwitteUser(react.state.nickname) ||
      react.state.nickname.trim().length < 4
    ) {
      Alert.alert('Invalid twitter username', '');
      return;
    }

    react.setState({
      loading: true,
    });

    axios
      .get(
        'https://api.twitter.com/2/users/by/username/' + react.state.nickname,
        {
          headers: {
            Authorization: 'Bearer ' + twitter_token,
          },
        },
      )
      .then(function (response) {
        console.log(response);

        if (response.data.hasOwnProperty('errors')) {
          if (response.data.errors.length > 0) {
            react.setState(
              {
                loading: false,
              },
              () => {
                Alert.alert(
                  response.data.errors[0].title,
                  response.data.errors[0].detail,
                );
              },
            );
          }
        } else {
          react.props.onSetUser(response.data.data);
        }
      })
      .catch(function (error) {
        console.log(error.response);
        react.setState(
          {
            loading: false,
          },
          () => {
            Alert.alert('Network error');
          },
        );
      });
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <KeyboardAvoidingView
          style={styles.container}
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
          <Text style={styles.title}>Demo Twitter App</Text>
          {!this.state.loading ? (
            <>
              <TextInput
                style={styles.input}
                value={this.state.nickname}
                onChangeText={text => {
                  this.setState({
                    nickname: text,
                  });
                }}
              />
              <TouchableOpacity
                style={styles.button}
                onPress={this.submit.bind(this)}>
                <Text style={styles.button_text}>Sign In</Text>
              </TouchableOpacity>
            </>
          ) : (
            <ActivityIndicator style={styles.loader} color={'#000000'} />
          )}
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
  },
  input: {
    width: width - 60,
    borderStyle: 'solid',
    borderColor: '#000000',
    borderWidth: 1,
    marginTop: 40,
    height: 40,
    marginLeft: 30,
    marginRight: 30,
    fontSize: 14,
    paddingLeft: 10,
    paddingRight: 10,
    padding: 0,
    color: '#000000',
  },
  button: {
    marginTop: 20,
    height: 40,
    marginLeft: 30,
    marginRight: 30,
    width: width - 60,
    backgroundColor: '#000000',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button_text: {
    color: '#ffffff',
    fontSize: 14,
  },
  loader: {
    marginTop: 30,
  },
});

export default connect(
  (state: {user: object}) => ({
    user: state.user,
  }),
  dispatch => ({
    onSetUser: (user: object) => {
      dispatch(setUser(user));
    },
  }),
)(LoginScreen);
