import React, {Component} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  View,
  ActivityIndicator,
} from 'react-native';

type TweetScreenProps = {
  route: any;
};

type TweetScreenState = {
  tweet: {
    id: string;
    text: string;
  };
};

class TweetScreen extends Component<TweetScreenProps, TweetScreenState> {
  constructor(props: TweetScreenProps) {
    super(props);
    this.state = {
      tweet: props.route.params.tweet,
    };
  }

  componentWillReceiveProps(nextProps: Readonly<TweetScreenProps>) {
    this.setState({
      tweet: nextProps.route.params.tweet,
    });
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.tweet}>
          <Text style={styles.tweet_id}>ID: {this.state.tweet.id}</Text>
          <Text style={styles.tweet_text}>Text: {this.state.tweet.text}</Text>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tweet: {
    borderColor: '#000000',
    borderStyle: 'solid',
    borderWidth: 1,
    width: '100%',
    padding: 20,
    marginBottom: 20,
  },
  tweet_id: {
    fontSize: 14,
  },
  tweet_text: {
    fontSize: 12,
  },
});

export default TweetScreen;
