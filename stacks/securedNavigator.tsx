import React, {Component, Fragment} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import SettingsScreen from '../screens/SettingsScreen';
import TweetsNavigator from './tweetsNavigator';

const Tab = createBottomTabNavigator();

export default class SecuredNavigator extends Component {
  render() {
    return (
      <Fragment>
        <Tab.Navigator>
          <Tab.Screen name="Home" component={TweetsNavigator} />
          <Tab.Screen name="Settings" component={SettingsScreen} />
        </Tab.Navigator>
      </Fragment>
    );
  }
}
