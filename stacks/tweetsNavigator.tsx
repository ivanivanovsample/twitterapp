import {createStackNavigator} from '@react-navigation/stack';
import React, {Component} from 'react';
import TweetsScreen from '../screens/TweetsScreen';
import TweetScreen from '../screens/TweetScreen';

const Stack = createStackNavigator();

export default class TweetsNavigator extends Component {
  render() {
    return (
      <Stack.Navigator>
        <Stack.Screen
          name="TweetsScreen"
          component={TweetsScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen name="TweetScreen" component={TweetScreen} />
      </Stack.Navigator>
    );
  }
}
