const defaultState = null;

export default function reducer(state = defaultState, action: { type: string; user: object; }) {
  switch (action.type) {
    case 'SET_USER':
      if (action.user === null) {
        return defaultState;
      } else {
        return Object.assign({}, state, action.user);
      }
    default:
      return state;
  }
}
